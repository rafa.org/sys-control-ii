# coding: utf-8
import serial
import time
import csv

import numpy # carrega a biblioteca numpy
import matplotlib.pyplot as plt #carrega a biblioteca pyplot
import matplotlib.gridspec as gridspec #carrega a biblioteca gridspec
from drawnow import * #carrega a biblioteca drawnow
from array import array #carrega a biblioteca array

#instanciação dos objectos
tempo = array('f')
sinal = array('f')
t = 0.04

# arduino serial
ser = serial.Serial(port='COM4', baudrate='9600')

plt.ion()
cnt = 0

def makeFig():
    #Plot 1
    plt.subplot(111)#posicao do subplot
    plt.ylim([-3,2.5])#valor min e max de y
    plt.title('titulo')#titulo
    plt.grid(True)
    plt.ylabel('Sinal')#etiquetas do eixo y
    plt.xlabel('Time (s)')
    plt.plot(tempo, sinal, 'ro-', label='Amostragem') #plot de temperature
    plt.legend(loc='upper left')#plot da legenda


X = [0] * 3
C =  [0] * 3
signalX = ""
signalC = 0

if(ser.isOpen()):
    while 1:
        if(ser.inWaiting() > 0):
            aux = ser.read()
            print(aux)
            if (aux == '*'):
                print("signalX")
                print(signalX)
                X.pop()
                X.insert(0,float(signalX))
                print(X)
                C.pop()
                C.insert(0,float(signalC))
                signalC = 9.11510*(10**-6)*X[0] + 3.11610*(10**-5)*X[1] + 6.61910*(10**-3)*X[2] + 2.457*C[0] - 1.984*C[1] + 0.5272*C[2]
                # C(z) = 9,11510-6X(z)z-1+3,11610-5X(z)z-2 +  6,61910-6X(z)z-3 + 2,457C(z)z-1 -1,984C(z)z-2+0,5272C(z)z-3
                print("signalC")
                print(signalC)
                signalC = format(signalC, '.10f')
                
                sinal.append(float(signalC))
                tempo.append(t)
                drawnow(makeFig)
                plt.pause(.000005)
                t = t + 0.04
                cnt = cnt + 1
                if(cnt>350):
                    sinal.pop(0)
                    tempo.pop(0)

                ser.write('B')

                for i in range(len(signalC)):
                    ser.write(signalC[i])
                    time.sleep(0.01)

                ser.write('C')

                with open("test_data.csv","a") as f :
                    writer = csv.writer(f , delimiter ="," )
                    writer.writerow([str(signalC)])

                signalX = ""

            else:
                signalX += aux

else:
    print("error")
